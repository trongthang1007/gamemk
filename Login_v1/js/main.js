
(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    function confirmPassword(e) {
        let password = $('#pw').val();
        let cfpassword = $('#cfpw').val();
        console.log(password);
        console.log(cfpassword);
        
        if(password !== cfpassword) {
            alert('Incorrect password!');
            e.preventDefault();
        }
    }

    $('.form-sub').submit(function(e){
        confirmPassword(e);
    });

    $('.form-login').submit(function(){
        location.replace('index.html');
    });
    $(document).ready(function() {
        var tablePlay = $('#tablePlay').DataTable({
            "bLengthChange": false,
            "pageLength": 5,
        });
        $('#tableTransaction').DataTable({
            "bLengthChange": false,
            "pageLength": 3,
        });

        $('#tablePlay tbody').on('click', 'tr', function () { 
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                tablePlay.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $('#tablePlay tbody').on('dblclick', 'tr', function() {
            $(location).attr('href', 'game/index.html');
        });
    } );

    $('.btn_tab1').click(function() {
        $('.transaction-tab').removeClass('active-table');
        $('#tablePlay_wrapper').removeClass('disactive-table');
        $('#tablePlay_wrapper').addClass('active-table');
    });

    $('.btn_tab2').click(function() {
        $('#tablePlay_wrapper').removeClass('active-table');
        $('#tablePlay_wrapper').addClass('disactive-table');
        $('.transaction-tab').addClass('active-table');
    });

})(jQuery);